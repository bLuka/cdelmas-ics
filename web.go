package main

import (
	"fmt"
	"log"
	"net/http"
	"time"

	ics "github.com/arran4/golang-ical"
)

// startAndRunWebserver registers routes and launch the webserver
func startAndRunWebserver(port, route string, records *[]record) {
	http.HandleFunc(route, serveCalendar(records))

	log.Printf("Listening on port %s", port)
	err := http.ListenAndServe(port, nil)
	log.Println("Error while starting webserver:", err)
}

// serveCalendar exposes an ICS interface (HTTP GET) over the given records cache
func serveCalendar(records *[]record) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		cal := ics.NewCalendar()
		cal.SetMethod(ics.MethodRequest)

		for _, r := range *records {
			if err := addEventFromRecord(cal.AddEvent(r.RecordID), r); err != nil {
				w.WriteHeader(http.StatusInternalServerError)
			}
		}

		if _, err := w.Write([]byte(cal.Serialize())); err != nil {
			log.Println("Error writing body:", err)
		}
	}
}

// addEventFromRecord converts the given record into an ics.VEvent
func addEventFromRecord(event *ics.VEvent, r record) error {
	startDate, err := time.Parse("2006-01-02", r.Fields.Date)
	if err != nil {
		return err
	}
	startTime, err := time.Parse("15:04", r.Fields.ClosingTime)
	if err != nil {
		return err
	}
	endTime, err := time.Parse("15:04", r.Fields.ReopeningTime)
	if err != nil {
		return err
	}

	eventDuration := time.Duration(endTime.Hour())*time.Hour + time.Duration(endTime.Minute())*time.Minute
	if endTime.Unix() < startTime.Unix() {
		eventDuration += time.Duration(24) * time.Hour
	}

	startDatetime := startDate.Add(time.Duration(startTime.Hour())*time.Hour + time.Duration(startTime.Minute())*time.Minute)
	endDatetime := startDate.Add(eventDuration)

	event.SetCreatedTime(r.Timestamp)
	event.SetStartAt(startDatetime)
	event.SetEndAt(endDatetime)
	event.SetSummary("Fermeture du pont Chaban-Delmas")
	event.SetLocation("Pont Jacques Chaban-Delmas, 33300 Bordeaux")
	event.SetDescription(fmt.Sprintf("Raison : %s", r.Fields.Boat))

	return nil
}
