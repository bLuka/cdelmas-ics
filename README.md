# Chaban Delmas bridge closures to ICS
This project is an API exposing Bordeaux's open data into an ICS-compliant route (calendar) for the Chaban Delmas bridge closures.

## Quick start

```Bash
go mod download
go run .
```

It will expose API port :8080 by default.

http://localhost:8080/bridge.ics now holds the Jacques Chaban-Delmas bridge calendar!

## Options

You can modify it by running it with the `--listen` flag:

```Bash
go run . --listen localhost:80
```

You can also change the listening route with the `--route` flag:

```Bash
go run . --route /jacques-chaban-delmas.ics
```

Run `--help` or `-h` for usage.

## Contributing

Feel free to open any issue if you notice anything wrong, want to add something, or need help.

You can also fork, start developping locally, and submit a merge request in order to bring a new feature or fix.
