package main

import (
	"flag"
	"time"
)

// syncInterval matches the waiting time before resyncing
const syncInterval = 5 * time.Minute

var (
	listenTo = flag.String("listen", ":8080", "The listening address in format address:port.")
	route    = flag.String("route", "/bridge.ics", "The route you want the API to listen to.")
)

// main starts the 2 services:
// - the records cache fetcher
// - the calendar HTTP server
func main() {
	var recordsCache []record

	flag.Parse()

	startEventFetchingLoop(syncInterval, &recordsCache)
	startAndRunWebserver(*listenTo, *route, &recordsCache)
}
