package main

import (
	"encoding/json"
	"log"
	"net/http"
	"os"
	"time"
)

// record defines the event during which the bridge is closed
type record struct {
	DatasetID string    `json:"datasetid"`
	RecordID  string    `json:"recordid"`
	Timestamp time.Time `json:"record_timestamp"`

	Fields struct {
		Date          string `json:"date_passage"`
		ClosingType   string `json:"type_de_fermeture"`
		ClosingFull   string `json:"fermeture_totale"`
		Boat          string `json:"bateau"`
		ClosingTime   string `json:"fermeture_a_la_circulation"`
		ReopeningTime string `json:"re_ouverture_a_la_circulation"`
	} `json:"fields"`
}

// startEventFetchingLoop fetches events, filling it in the given records cache (and refreshes it at given interval)
func startEventFetchingLoop(interval time.Duration, records *[]record) {
	go (func() {
		if err := getBridgeEvents(records); err != nil {
			log.Println("Cannot get events:", err)
			os.Exit(1)
		}

		for range time.After(interval) {
			if err := getBridgeEvents(records); err != nil {
				log.Println(err)
			}
		}
	})()
}

// getBridgeEvents fills record cache with updated entries (retrieved from API)
func getBridgeEvents(records *[]record) error {
	var response struct {
		Hits       uint `json:"nhits"`
		Parameters struct {
			Dataset  string   `json:"dataset"`
			Timezone string   `json:"timezone"`
			Rows     uint     `json:"rows"`
			Start    uint     `json:"start"`
			Format   string   `json:"format"`
			Facet    []string `json:"facet"`
		} `json:"parameters"`

		Records     []record `json:"records"`
		FacetGroups []struct {
			Facets []map[string]interface{} `json:"facets"`
			Name   string                   `json:"name"`
		} `json:"facet_groups"`
	}

	url := "https://opendata.bordeaux-metropole.fr/api/records/1.0/search/?dataset=previsions_pont_chaban"
	method := "GET"

	client := &http.Client{}
	req, err := http.NewRequest(method, url, nil)
	if err != nil {
		return err
	}

	res, err := client.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	if err := json.NewDecoder(res.Body).Decode(&response); err != nil {
		return err
	}

	*records = response.Records

	return nil
}
